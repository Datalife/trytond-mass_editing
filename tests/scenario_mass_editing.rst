=====================
Mass Editing Scenario
=====================

Imports::

    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules
    >>> from decimal import Decimal
    >>> import datetime


Install stock_supply Module::

    >>> config = activate_modules(['mass_editing', 'product_attribute'])

Create today::

    >>> today = datetime.date.today()

Create product attribute::

    >>> ProductAttribute = Model.get('product.attribute')

    >>> product_attribute = ProductAttribute()
    >>> product_attribute.name = 'attr_number'
    >>> product_attribute.string = 'Attribute number'
    >>> product_attribute.type_ = 'numeric'
    >>> product_attribute.save()

    >>> product_attribute2 = ProductAttribute()
    >>> product_attribute2.name = 'attr_date'
    >>> product_attribute2.string = 'Attribute date'
    >>> product_attribute2.type_ = 'date'
    >>> product_attribute2.save()

    >>> ProductAttributeSet = Model.get('product.attribute.set')
    >>> attribute_set = ProductAttributeSet()
    >>> attribute_set.name = 'Set of attributes'
    >>> attribute_set.attributes.extend([product_attribute, product_attribute2])
    >>> attribute_set.save()

Create product::

    >>> Template = Model.get('product.template')
    >>> Uom = Model.get('product.uom')
    >>> unit, = Uom.find([('name', '=', 'Unit')])

    >>> template = Template()
    >>> template.name = 'Product 1'
    >>> template.default_uom = unit
    >>> template.type = 'goods'
    >>> template.list_price = Decimal('10.0')
    >>> template.attribute_set = attribute_set
    >>> template.save()
    >>> product, = template.products
    >>> product.attributes = {
    ...     product_attribute.name: Decimal('5.0'),
    ...     product_attribute2.name: today}
    >>> product.save()

    >>> template2 = Template()
    >>> template2.name = 'Product 2'
    >>> template2.default_uom = unit
    >>> template2.type = 'goods'
    >>> template2.list_price = Decimal('10.0')
    >>> template2.attribute_set = attribute_set
    >>> template2.save()
    >>> product2, = template2.products
    >>> product2.attributes = {
    ...     product_attribute.name: Decimal('5.0'),
    ...     product_attribute2.name: today}
    >>> product2.save()

Create mass editing::

    >>> MassEditing = Model.get('mass.editing')
    >>> ModelField = Model.get('ir.model.field')
    >>> IrModel = Model.get('ir.model')

    >>> model_product, = IrModel.find([
    ...     ('model', '=', 'product.product')])
    >>> field_attributes, = ModelField.find([
    ...     ('module', '=', 'product_attribute'),
    ...     ('model', '=', model_product),
    ...     ('name', '=', 'attributes')])

    >>> mass_editing = MassEditing()
    >>> mass_editing.model = model_product
    >>> mass_editing.model_fields.append(field_attributes)
    >>> mass_editing.save()
    >>> mass_editing.click('create_keyword')

wizard mass edigin::

    >>> with config.set_context(active_model='product.product'):
    ...     wizard = Wizard('mass.editing.wizard', [product, product2],
    ...         action={'id': mass_editing.keyword.action.id})

